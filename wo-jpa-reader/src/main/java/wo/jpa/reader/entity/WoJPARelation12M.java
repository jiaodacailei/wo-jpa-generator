package wo.jpa.reader.entity;

import java.lang.reflect.Field;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import wo.jpa.reader.util.JPAReaderUtil;

public class WoJPARelation12M extends WoJPARelation {
	private final static Logger LOG = LogManager.getLogger(WoJPARelation12M.class);

	private JoinColumn joinColumn;
	
	private String mappedBy;
	
	public WoJPARelation12M(Class<?> sourceClass, Field f) {
		Class<?> targetClass = this.getTargetType(f);
		LOG.info("targetClass:" + targetClass.getName());
		this.setSource(new WoJPAEntity(sourceClass));
		this.setTarget(new WoJPAEntity(targetClass));
		this.setSourceFieldName(f.getName());
		OneToMany m = f.getDeclaredAnnotation(OneToMany.class);
		this.mappedBy = m.mappedBy();
		Field tf = this.getField(targetClass, mappedBy);
		this.setTargetFieldName(tf.getName());
		joinColumn = tf.getDeclaredAnnotation(JoinColumn.class);
	}

	public JoinColumn getJoinColumn() {
		return joinColumn;
	}

	public void setJoinColumn(JoinColumn joinColumn) {
		this.joinColumn = joinColumn;
	}

	public String getMappedBy() {
		return mappedBy;
	}

	public void setMappedBy(String mappedBy) {
		this.mappedBy = mappedBy;
	}
	
}
