package wo.jpa.reader.entity;
import java.lang.reflect.Field;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class WoJPARelation121 extends WoJPARelation {
	private final static Logger LOG = LogManager.getLogger(WoJPARelation121.class);
	
	private JoinColumn joinColumn;
	
	/**
	 * true表示source实体维护关系数据，否则target实体维护关系数据
	 */
	private Boolean main = true;
	
	public WoJPARelation121 (Class<?>sourceClass, Field f) {
		Class<?> targetClass = this.getTargetType(f);
		this.setSource(new WoJPAEntity(sourceClass));
		this.setTarget(new WoJPAEntity(targetClass));
		this.setSourceFieldName(f.getName());
		joinColumn = f.getDeclaredAnnotation(JoinColumn.class);
		if (joinColumn == null) {
			OneToOne m = f.getDeclaredAnnotation(OneToOne.class);
			Field tf = this.getField(targetClass, m.mappedBy());
			this.setTargetFieldName(tf.getName());
			joinColumn = tf.getDeclaredAnnotation(JoinColumn.class);
			main = false;
		}
	}

	public JoinColumn getJoinColumn() {
		return joinColumn;
	}

	public void setJoinColumn(JoinColumn joinColumn) {
		this.joinColumn = joinColumn;
	}

	public Boolean getMain() {
		return main;
	}

	public void setMain(Boolean main) {
		this.main = main;
	}
	
}
