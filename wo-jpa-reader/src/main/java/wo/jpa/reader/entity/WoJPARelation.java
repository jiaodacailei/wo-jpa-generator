package wo.jpa.reader.entity;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import wo.common.exception.WoException;
import wo.common.util.WoUtil;
import wo.jpa.reader.util.JPAReaderConstant;
import wo.jpa.reader.util.JPAReaderUtil;

public class WoJPARelation {
	
	private final static Logger LOG = LogManager.getLogger(WoJPARelation.class);
	
	private WoJPAEntity source;
	
	private WoJPAEntity target;

	/**
	 * 本方字段名.
	 */
	private String sourceFieldName;
	
	/**
	 * 本方字段注释.
	 */
	private String sourceFieldComment = "";
	
	private String targetFieldName;
	
	/**
	 * 关联的关系
	 */
	protected WoJPARelation relatedRelation;
	
	public String getSourceFieldName() {
		return sourceFieldName;
	}

	/**
	 * @return
	 */
	public String getSourceFieldNameUpperFirstChar () {
		return WoUtil.getUpperFirstChar(this.sourceFieldName);
	}
	
	public void setSourceFieldName(String sourceFieldName) {
		this.sourceFieldName = sourceFieldName;
	}

	public String getTargetFieldName() {
		return targetFieldName;
	}

	public String getSourceFieldComment() {
		return sourceFieldComment;
	}
	
	/**
	 * 从注释中获取中文名.
	 * @return
	 */
	public String getSourceFieldCN() {
		return JPAReaderUtil.getNameInComment(sourceFieldComment, sourceFieldName);
	}
	
	public void setSourceFieldComment(String sourceFieldComment) {
		this.sourceFieldComment = sourceFieldComment;
	}

	/**
	 * @return
	 */
	public String getTargetFieldNameUpperFirstChar () {
		return WoUtil.getUpperFirstChar(this.targetFieldName);
	}
	
	public void setTargetFieldName(String targetFieldName) {
		this.targetFieldName = targetFieldName;
	}

	public WoJPAEntity getSource() {
		return source;
	}

	public void setSource(WoJPAEntity source) {
		this.source = source;
		this.source.setContext(this);
	}

	public WoJPAEntity getTarget() {
		return target;
	}

	public void setTarget(WoJPAEntity target) {
		this.target = target;
		this.target.setContext(this);
	}
	
	public WoJPARelation getRelatedRelation() {
		return relatedRelation;
	}

	public void setRelatedRelation(WoJPARelation relatedRelation) {
		this.relatedRelation = relatedRelation;
	}

	/**
	 * @param f
	 * @return
	 */
	protected Class<?> getTargetType (Field f) {
		Class<?> targetClass = null;
		if (Collection.class.isAssignableFrom(f.getType())) {
			// 如果是List类型，得到其Generic的类型    
	        Type genericType = f.getGenericType();   
	        // 如果是泛型参数的类型     
	        if(genericType != null && genericType instanceof ParameterizedType){     
	            ParameterizedType pt = (ParameterizedType) genericType;  
	            //得到泛型里的class类型对象    
	            targetClass = (Class<?>)pt.getActualTypeArguments()[0];   
	        }
		} else {
			targetClass = f.getType();
		}
		if (targetClass == null) {
			throw new WoException (JPAReaderConstant.ERROR_TARGET, f.getName());
		}
		return targetClass;
	}
	
	/**
	 * @param cls
	 * @param fieldName
	 * @return
	 */
	protected Field getField (Class<?>cls, String fieldName) {
		Field tf;
		try {
			tf = cls.getDeclaredField(fieldName);
		} catch (Exception e) {
			throw new WoException (e, JPAReaderConstant.ERROR_FIELD, fieldName);
		}
		return tf;
	}
	
	/**
	 * 是否自关联
	 * @return
	 */
	public Boolean relateSelf () {
		return this.source.getEntityClass().equals(this.target.getEntityClass());
	}
	
	/**
	 * 是否关联弱实体
	 * @return
	 */
	public Boolean relateWeakEntity () {
		return "true".equals(JPAReaderUtil.getParamsInComment(getSourceFieldComment()).get("relateWeakEntity"));
	}
	
	public static void main (String[] args) {
		LOG.info("2323。sfsf".indexOf('。'));
	}
}
