package wo.jpa.reader.entity;

import java.lang.reflect.Field;

import javax.persistence.JoinColumn;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class WoJPARelationM21 extends WoJPARelation {
	private final static Logger LOG = LogManager.getLogger(WoJPARelationM21.class);

	private JoinColumn joinColumn;

	private Boolean lazy = false;
	
	public WoJPARelationM21(Class<?> sourceClass, Field f) {
		LOG.info("sourceClass:" + sourceClass.getName());
		this.setSourceFieldName(f.getName());
		Class<?> targetClass = this.getTargetType(f);
		LOG.info("targetClass:" + targetClass.getName());
		this.setSource(new WoJPAEntity(sourceClass));
		this.setTarget(new WoJPAEntity(targetClass));
		joinColumn = f.getDeclaredAnnotation(JoinColumn.class);
	}

	public JoinColumn getJoinColumn() {
		return joinColumn;
	}

	public void setJoinColumn(JoinColumn joinColumn) {
		this.joinColumn = joinColumn;
	}

	public Boolean getLazy() {
		return lazy;
	}

	public void setLazy(Boolean lazy) {
		this.lazy = lazy;
	}
	
	public WoJPARelation getRelatedRelation() {
		if (relatedRelation != null) {
			return relatedRelation;
		}
		for (WoJPARelation r : this.getTarget().getRelations12M()) {
			WoJPARelation12M r2 = (WoJPARelation12M)r;
			if (r2.getMappedBy().equals(this.getSourceFieldName())) {
				relatedRelation = r2;
			}
		}
		return relatedRelation;
	}
}
