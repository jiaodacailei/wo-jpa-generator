package wo.jpa.reader.util;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import wo.common.exception.WoResultCode;

public class JPAReaderConstant {
	private final static Logger LOG = LogManager.getLogger(JPAReaderConstant.class);
	
	public final static WoResultCode ERROR_CLASS = new WoResultCode(100, "加载类【%s】出错！");
	
	public final static WoResultCode ERROR_TARGET = new WoResultCode(101, "未找到属性【%s】的关联实体类！");
	
	public final static WoResultCode ERROR_FIELD = new WoResultCode(102, "未找到属性【%s】！");
	
	public final static WoResultCode ERROR_M2M = new WoResultCode(103, "属性【%s】的@ManyToMany(mappedBy)和@JoinTable均未定义！");
	
	public final static WoResultCode ERROR_JOINTABLE = new WoResultCode(104, "属性【%s】的@JoinTable未定义！");
}
