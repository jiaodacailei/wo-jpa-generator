package wo.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.anakia.Escape;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.util.ResourceUtils;

import wo.common.exception.WoException;

/**
 * The summery of the class.
 * 
 * @version 1.0
 * @author cailei
 */
public class WoVelocityUtil {

	/**
	 * Recording the log of this class.
	 */
	private final static Logger LOG = LogManager.getLogger(WoVelocityUtil.class);

	private final static String ENCODING_UTF8 = "UTF-8";

	private static VelocityEngine velocity = new VelocityEngine();

	/**
	 * 
	 */
	static {
		Properties p = new Properties();
		try {
			p.setProperty("file.resource.loader.path",
					ResourceUtils.getFile("classpath:velocity").getAbsolutePath());
			// p.setProperty("runtime.references.strict", "true");
			velocity.init(p);
		} catch (Exception e) {
			LOG.warn("Init velocity failed!", e);
		}
	}

	private static Template getTemplate(String tmplFileName) {
		Template template = null;
		try {
			template = velocity.getTemplate(tmplFileName, ENCODING_UTF8);
		} catch (Exception e1) {
			throw new WoException(e1, WoConstant.ERROR_VELOCITY_TMPL, tmplFileName);
		}
		return template;
	}

	private static VelocityContext getContext(Map<String, Object> map) {
		VelocityContext context = new VelocityContext();
		if (map == null || map.size() == 0) {
			return context;
		}
		for (String key : map.keySet()) {
			context.put(key, map.get(key));
		}

		return context;
	}

	/**
	 * @param tmplFileName
	 * @param context
	 * @return
	 * @throws CrudException
	 */
	public static String mergeToString(String tmplFileName, Map<String, Object> map) {
		VelocityContext context = getContext(map);
		Template template = getTemplate(tmplFileName);
		StringWriter writer = null;
		try {
			writer = new StringWriter();
			template.merge(context, writer);
			return writer.getBuffer().toString();
		} catch (Exception ex) {
			throw new WoException(ex, WoConstant.ERROR_VELOCITY_TMPL_STR, tmplFileName);
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * @param tmplFilePath
	 * @param srcOutputStream
	 * @param context
	 * @throws CrudException
	 */
	private static void mergeToOutputStream(String tmplFilePath, OutputStream srcOutputStream, Map<String, Object> map) {
		VelocityContext context = getContext(map);
		Template template = getTemplate(tmplFilePath);
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(srcOutputStream, ENCODING_UTF8);
			template.merge(context, writer);
		} catch (Exception ex) {
			throw new WoException(ex, WoConstant.ERROR_VELOCITY_TMPL_OUTSTREAM, tmplFilePath);
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * @param tmplFilePath
	 * @param srcFilePath
	 * @param context
	 * @throws CrudException
	 */
	public static void mergeToFile(String tmplFilePath, String srcFilePath, Map<String, Object> map) {
		LOG.info("srcFilePath:" + srcFilePath);
		File srcFile = new File(srcFilePath);
		srcFile.getParentFile().mkdirs();
		try {
			if (!srcFile.exists()) {
				srcFile.createNewFile();
			}
			mergeToOutputStream(tmplFilePath, new FileOutputStream(srcFile), map);
		} catch (IOException e) {
			throw new WoException(e, WoConstant.ERROR_VELOCITY_TMPL_FILE, tmplFilePath, srcFilePath);
		}
	}

	/**
	 * @param request
	 * @param tmplString
	 * @param allowEmpty
	 * @return
	 */
	public static String parse(String tmplString, Map<String, Object> map) {
		if (WoUtil.isEmpty(tmplString)) {
			return null;
		}
		VelocityContext context = getContext(map);
		StringWriter sw = null;
		try {
			sw = new StringWriter();
			velocity.evaluate(context, sw, "xxx", tmplString);
			LOG.info("parseResult:" + sw.toString());
			return sw.toString().trim();
		} catch (Exception e) {
			throw new WoException(e, WoConstant.ERROR_VELOCITY_TMPLSTR, tmplString);
		} finally {
			try {
				sw.close();
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * @param s
	 * @return
	 */
	public static String getText(String s) {
		if (s == null) {
			return "";
		}
		return Escape.getText(s);
	}
	
	public static void main (String[] args) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("module", "SysStaff");
		LOG.info(mergeToString("staff-list.html", map));
	}
}
