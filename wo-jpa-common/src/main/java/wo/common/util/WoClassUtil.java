package wo.common.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import wo.common.exception.WoException;

public class WoClassUtil {
	
	private final static Logger LOG = LogManager.getLogger(WoClassUtil.class);
	
	 /**
     * 该方法会得到所有的类，将类的绝对路径写入到classPaths中
     * @param file
     */
    private static void doPath(String classPath, File file, List<Class> classPaths) {
        if (file.isDirectory()) {// 文件夹
            // 文件夹我们就递归
            File[] files = file.listFiles();
            for (File f1 : files) {
                doPath(classPath, f1, classPaths);
            }
        } else {// 标准文件
            // 标准文件我们就判断是否是class文件
            if (file.getName().endsWith(".class")) {
            	String s = file.getPath().replace('\\', '/');
            	LOG.info(s);
            	LOG.info(classPath);
            	s = s.replace(classPath, "").replace('/', '.').replace(".class","");
                // 如果是class文件我们就放入我们的集合中。
                try {
					classPaths.add(Class.forName(s));
				} catch (ClassNotFoundException e) {
					throw new WoException (e, WoConstant.ERR_CLASS, s);
				}
            }
        }
    }
    
    /**
     * windows的classPath中包含冒号，需要去掉其第一个字符反斜杠。
     * @param classPath
     * @return
     */
    private static String getClassPath (String projectDir) {
    	// String classPath = WoClassUtil.class.getResource("/").getPath();
    	String classPath = projectDir + "target/classes/";
//    	if (classPath.indexOf(':') > 0) {
//    		return classPath.substring(1);
//    	}
    	return classPath;
    }
    
    /**
     * 获取包下的类
     * @param pkgToScan
     * @param pkgToScan2 
     * @return
     */
    public static List<Class> findClassesByPackage (String projectDir, String pkgToScan) {
    	//先把包名转换为路径,首先得到项目的classpath
        String classPath = getClassPath (projectDir);
        LOG.info("classpath:" + classPath);
        //然后把我们的包名basePath转换为路径名
        pkgToScan = pkgToScan.replace(".", File.separator);
        //然后把classpath和basePack合并
        String searchPath = classPath + pkgToScan;
        LOG.info("searchPath:" + searchPath);
    	List<Class> classFullNames = new ArrayList<Class>();
        WoClassUtil.doPath(classPath, new File(searchPath), classFullNames);
        return classFullNames;
    }
    
    public static void main (String[] args) {
    	LOG.info(findClassesByPackage ("./", "wo.common.util"));
    }
}
