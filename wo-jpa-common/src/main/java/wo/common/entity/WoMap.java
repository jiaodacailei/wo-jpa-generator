package wo.common.entity;
import java.util.HashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * mybatis的数据对象
 * @author cailei
 *
 */
public class WoMap extends HashMap<String, Object> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1893804106036973411L;

	private final static Logger LOG = LogManager.getLogger(WoMap.class);
	
	public final static String WHERE = "woWhere";
	
	public final static String ORDERBY = "woOrderBy";
	
	public final static String START = "woStart";
	
	public final static String LIMIT = "woLimit";
	
	public void setWhere (Object val) {
		put (WHERE, val);
	}
	
	public void setOrderBy (Object val) {
		put (ORDERBY, val);
	}
	
	public void setPageStart (Object val) {
		put (START, val);
	}
	
	public void setPageLimit (Object val) {
		put (LIMIT, val);
	}
}
