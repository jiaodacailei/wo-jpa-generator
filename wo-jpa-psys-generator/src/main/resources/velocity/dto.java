package ${entity.getSiblingPackage("dto")};

import java.util.ArrayList;
import java.util.List;

import com.qfedu.common.entity.WoPage;
import ${entity.entityClass};

/**
 * PO实体${entity.entityName}对应的DTO类.
 * @author cailei
 */
public class ${entity.entityName}Dto extends ${entity.entityName}Dto_ {

	/**
	 * 无参构造函数
	 */
	public ${entity.entityName}Dto() {
		super ();
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public ${entity.entityName}Dto(${entity.entityName} po) {
		super (po);
	}
	
	/**
	 * 将po列表数据转化为dto列表数据
	 * @param pos
	 * @return
	 */
	public static List<${entity.entityName}Dto> getDtos (List<${entity.entityName}> pos) {
		List<${entity.entityName}Dto> rs = new ArrayList<${entity.entityName}Dto>();
		for (${entity.entityName} r : pos) {
			${entity.entityName}Dto dto = new ${entity.entityName}Dto(r);
			rs.add(dto);
		}
		return rs;
	}
	
	/**
	 * 将分页po数据转化为dto分页数据
	 * @param pos
	 * @param total
	 * @return
	 */
	public static WoPage<${entity.entityName}Dto> getPageData(List<${entity.entityName}> pos, Long total) {
		WoPage<${entity.entityName}Dto> puDto = new WoPage<${entity.entityName}Dto>(getDtos(pos), total);
		return puDto;
	}
}
