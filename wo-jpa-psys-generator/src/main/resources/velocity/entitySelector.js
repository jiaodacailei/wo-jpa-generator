/**
 * ${entity.entityCN}选择器初始化，该方法需要在选择器加载完成后调用.
 * @single 选择器是否单选
 * */
function init${entity.moduleUpperFirstChar}${entity.entityName}Selector(single) {
	
	var selectorModule = '${entity.moduleUpperFirstChar}${entity.entityName}Selector';
	var selectedIds = $("#form" + selectorModule + "List input[name='selectedIds']").val();
	// 定义加载选择器列表的函数
	function loadSelectorList (options) {
		PSys.loadList(Object.assign({
			module : selectorModule,
			url : '/${entity.module}/${entity.entityNameLowerFirstChar}/selector/list',
			selectedIds : selectedIds
		}, options));
	}
	
	// 加载选择器列表
	loadSelectorList();
	
	// 设置查询按钮点击事件
	$('#div' + selectorModule + 'List').on ('click', '#btn' + selectorModule + 'Search', function () {
		loadSelectorList();
	});
	
	// 确定按钮点击事件
	$('#div' + selectorModule + 'List').on ('click', '#btn' + selectorModule + 'OK', function () {
		PSys.selectSelectorOK(selectorModule);
	});
	// 选择器列表的行上checkbox点击事件
	$('#div' + selectorModule + 'List').on ('click', ".table input[type=checkbox]", function () {
		var opts = {
			id : this.value,
			name : $(this).parent().next().next().text(),
			checked : $(this).is(':checked'),
			module : selectorModule
		};
		if (single) {
			PSys.selectSingleSelector(opts);
		} else {
			PSys.selectMultipleSelector(opts);
		}
	});
	// 设置上一页按钮点击事件
	$('body').on ('click', '#btn' + selectorModule + 'LastPage', function () {
		loadSelectorList({
			pageOffset : -1
		});
	});
	// 设置下一页按钮点击事件
	$('body').on ('click', '#btn' + selectorModule + 'NextPage', function () {
		loadSelectorList({
			pageOffset : 1
		});
	});
	// 设置某一页按钮点击事件
	$('#div' + selectorModule + 'List').on ('click', ".button-group input[type=radio]", function () {
		loadSelectorList();
	});
}