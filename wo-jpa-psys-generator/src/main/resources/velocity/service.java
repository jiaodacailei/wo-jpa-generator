package ${entity.getSiblingPackage("service")};

import com.qfedu.common.entity.WoPage;
import ${entity.getSiblingPackage("dto")}.${entity.entityName}Dto;
import com.qfedu.psys.dto.WoUser;

/**
 * PO实体${entity.entityName}对应的Service接口.
 * @author cailei
 */
public interface ${entity.entityName}Service {
	#set ($idType = ${entity.keys.get(0).fieldType.name})
	
	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	WoPage<${entity.entityName}Dto> getPageData(WoUser currentUser, String searchContent, Long page, Long size);
	
	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	void create(WoUser currentUser, ${entity.entityName}Dto dto);

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	${entity.entityName}Dto getById($idType id);
	
	/**
	 * 更新对象
	 * @param dto
	 */
	void update(${entity.entityName}Dto dto);
	
	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	void delete($idType[] ids);
}
