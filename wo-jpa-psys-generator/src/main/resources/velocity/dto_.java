package ${entity.getSiblingPackage("dto")};

import java.util.ArrayList;
import java.util.List;
import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
#foreach ($cls in $entity.relatedEntityClasses)
import $cls;
#end

/**
 * PO实体${entity.entityName}对应的DTO父类.这是自动生成的代码,请不要修改.如要添加属性或者方法,请在其子类DTO中修改.
 * @author cailei
 */
class ${entity.entityName}Dto_ {

#foreach($col in $entity.keys)
	/**
	 * 主键${col.fieldName}
	 */
	private ${col.fieldType.name} ${col.fieldName};
#end

#foreach($col in $entity.columns)
	/**
	 * 属性${col.fieldName}
	 */
	private ${col.fieldType.name} ${col.fieldName};
#end

#foreach ($r in $entity.relationsM2M) 
	/**
	 * 对应PO的${r.sourceFieldName}属性,多对多关联实体${r.target.entityName}的主键值,如有多个以逗号隔开
	 */
	private String ${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar} = "";

	/**
	 * 对应PO的${r.sourceFieldName}属性,多对多关联实体${r.target.entityName}的${r.target.columns.get(0).fieldName}属性值,如有多个以逗号隔开
	 */
	private String ${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar} = "";
#end
	
#foreach ($r in $entity.relationsM21) 
	/**
	 * 对应PO的${r.sourceFieldName}属性,多对一关联实体${r.target.entityName}的主键值
	 */
	private ${r.target.keys.get(0).fieldType.name} ${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar};

	/**
	 * 对应PO的${r.sourceFieldName}属性,多对一关联实体${r.target.entityName}的${r.target.columns.get(0).fieldName}属性值
	 */
	private ${r.target.columns.get(0).fieldType.name} ${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar};
#end

#foreach ($r in $entity.relations12M) 
	/**
	 * 对应PO的${r.sourceFieldName}属性,一对多关联实体${r.target.entityName}的主键值
	 */
	private List<${r.target.entityName}Dto> ${r.sourceFieldName} = new ArrayList<${r.target.entityName}Dto>();
#end	
	
	/**
	 * 无参构造函数
	 */
	public ${entity.entityName}Dto_() {
	}

	/**
	 * 构造函数,通过po构造dto
	 */
	public ${entity.entityName}Dto_(${entity.entityName} po) {
	#foreach($col in $entity.keys)
		// 设置主键${col.fieldName}
		this.${col.fieldName} = po.${col.getterName}();
	#end
	#foreach($col in $entity.columns)
		// 设置属性${col.fieldName}
		this.${col.fieldName} = po.${col.getterName}();
	#end
	
	#foreach ($r in $entity.relationsM2M)
		#set ($id = "${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}")
		#set ($name = "${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}")
		// 设置DTO的${r.sourceFieldName}属性值
		for (${r.target.entityName} p : po.get${r.sourceFieldNameUpperFirstChar}()) {
			if (!"".equals(${id})) {
				${id} += ",";
				${name} += ",";
			}
			${id} += p.${r.target.keys.get(0).getterName}();
			${name} += p.${r.target.columns.get(0).getterName}();
		}
	#end
	#foreach ($r in $entity.relationsM21)
		// 设置DTO的${r.sourceFieldName}属性值
		if (po.get${r.sourceFieldNameUpperFirstChar}() != null) {
			this.${r.sourceFieldName}Id = po.get${r.sourceFieldNameUpperFirstChar}().get${r.target.keys.get(0).fieldNameUpperFirstChar}();
			this.${r.sourceFieldName}Name = po.get${r.sourceFieldNameUpperFirstChar}().get${r.target.columns.get(0).fieldNameUpperFirstChar}();
		}
	#end
	#foreach ($r in $entity.relations12M)
		// 设置DTO的${r.sourceFieldName}属性值
		this.${r.sourceFieldName} = ${r.target.entityName}Dto.getDtos(po.get${r.sourceFieldNameUpperFirstChar}());
	#end
	}

#foreach($col in $entity.keys)
	
	/**
	 * 主键${col.fieldName}的getter方法
	 */
	public ${col.fieldType.name} ${col.getterName}() {
		return this.${col.fieldName};
	}

	/**
	 * 主键${col.fieldName}的setter方法
	 */
	public void ${col.setterName}(${col.fieldType.name} ${col.fieldName}) {
		this.${col.fieldName} = ${col.fieldName};
	}
#end

#foreach($col in $entity.columns)
	
	/**
	 * 属性${col.fieldName}的getter方法
	 */
	public ${col.fieldType.name} ${col.getterName}() {
		return this.${col.fieldName};
	}

	/**
	 * 属性${col.fieldName}的setter方法
	 */
	public void ${col.setterName}(${col.fieldType.name} ${col.fieldName}) {
		this.${col.fieldName} = ${col.fieldName};
	}
#end	

#foreach ($r in $entity.relationsM2M)
	#set ($id = "${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}")
	#set ($name = "${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}")
	#set ($id2 = "${r.sourceFieldNameUpperFirstChar}${r.target.keys.get(0).fieldNameUpperFirstChar}")
	#set ($name2 = "${r.sourceFieldNameUpperFirstChar}${r.target.columns.get(0).fieldNameUpperFirstChar}")
	/**
	 * 属性$id的getter方法
	 */
	public String get${id2}() {
		return ${id};
	}

	/**
	 * 属性$id的setter方法
	 */
	public void set${id2}(String ${id}) {
		this.${id} = ${id};
	}

	/**
	 * 属性$name的getter方法
	 */
	public String get${name2}() {
		return ${name};
	}

	/**
	 * 属性$name的setter方法
	 */
	public void set${name2}(String ${name}) {
		this.${name} = ${name};
	}
#end
#foreach ($r in $entity.relationsM21)
	#set ($id = "${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}")
	#set ($name = "${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}")
	#set ($id2 = "${r.sourceFieldNameUpperFirstChar}${r.target.keys.get(0).fieldNameUpperFirstChar}")
	#set ($name2 = "${r.sourceFieldNameUpperFirstChar}${r.target.columns.get(0).fieldNameUpperFirstChar}")
	#set ($idType = ${r.target.keys.get(0).fieldType.name})
	#set ($nameType = ${r.target.columns.get(0).fieldType.name})
	/**
	 * 属性$id的getter方法
	 */
	public $idType get${id2}() {
		return ${id};
	}

	/**
	 * 属性$id的setter方法
	 */
	public void set${id2}($idType ${id}) {
		this.${id} = ${id};
	}

	/**
	 * 属性$name的getter方法
	 */
	public $nameType get${name2}() {
		return ${name};
	}

	/**
	 * 属性$name的setter方法
	 */
	public void set${name2}($nameType ${name}) {
		this.${name} = ${name};
	}
#end
#foreach ($r in $entity.relations12M)
	/**
	 * 属性${r.sourceFieldName}的setter方法
	 */
	public List<${r.target.entityName}Dto> get${r.sourceFieldNameUpperFirstChar}() {
		return ${r.sourceFieldName};
	}

	/**
	 * 属性${r.sourceFieldName}的setter方法
	 */
	public void set${r.sourceFieldNameUpperFirstChar}(List<${r.target.entityName}Dto> ${r.sourceFieldName}) {
		this.${r.sourceFieldName} = ${r.sourceFieldName};
	}
#end

	/**
	 * 将当前对象转化为po
	 * @return
	 */
	public ${entity.entityName} createPO() {
		${entity.entityName} po = new ${entity.entityName}();
	#foreach($col in $entity.keys)
		// 设置PO主键${col.fieldName}
		#if (${col.fieldType.name} == "java.lang.String")
			if (WoUtil.isEmpty(this.${col.fieldName})) {
				po.${col.setterName}(java.util.UUID.randomUUID().toString());
			} else {
				po.${col.setterName}(this.${col.fieldName});
			}
		#else
			po.${col.setterName}(this.${col.fieldName});
		#end
	#end
	#foreach($col in $entity.columns)
		// 设置PO属性${col.fieldName}
		#if (${col.fieldName} == "createTime") 
		po.${col.setterName}(new java.util.Date());
		#else
		po.${col.setterName}(this.${col.fieldName});
		#end
	#end
	
	#foreach ($r in $entity.relationsM2M)
		#set ($id = "${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}")
		#set ($name = "${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}")
		// 设置PO的${r.sourceFieldName}属性值
		List<${r.target.entityName}> ${r.sourceFieldName} = new ArrayList<${r.target.entityName}>();
		String[] ${id}Array = WoUtil.splitIds($id);
		for (String id : ${id}Array) {
			${r.target.entityName} p = new ${r.target.entityName} ();
			p.${r.target.keys.get(0).setterName}(id);
			${r.sourceFieldName}.add(p);
		}
		po.set${r.sourceFieldNameUpperFirstChar}(${r.sourceFieldName});
	#end
	#foreach ($r in $entity.relationsM21)
		#set ($id = "${r.target.keys.get(0).fieldNameUpperFirstChar}")
		// 设置关系数据
		${r.target.entityName} ${r.sourceFieldName} = new ${r.target.entityName}();
		if (!WoUtil.isEmpty(${r.sourceFieldName}${id})) {
			${r.sourceFieldName}.set${id}(this.${r.sourceFieldName}${id});
			po.set${r.sourceFieldNameUpperFirstChar}(${r.sourceFieldName});
		}
	#end
		return po;
	}

	/**
	 * @param po
	 */
	public void updatePO(${entity.entityName} po) {
	#foreach($col in $entity.columns)
		// 设置PO属性${col.fieldName}
		po.${col.setterName}(this.${col.fieldName});
	#end
	
	#foreach ($r in $entity.relationsM2M)
		#set ($id = "${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}")
		#set ($name = "${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}")
		// 设置PO的${r.sourceFieldName}属性值:M2M
		List<${r.target.entityName}> ${r.sourceFieldName} = new ArrayList<${r.target.entityName}>();
		String[] ${id}Array = WoUtil.splitIds($id);
		for (String id : ${id}Array) {
			${r.target.entityName} p = new ${r.target.entityName} ();
			p.${r.target.keys.get(0).setterName}(id);
			${r.sourceFieldName}.add(p);
		}
		po.set${r.sourceFieldNameUpperFirstChar}(${r.sourceFieldName});
	#end
	#foreach ($r in $entity.relationsM21)
		#set ($id = "${r.target.keys.get(0).fieldNameUpperFirstChar}")
		// 设置PO的${r.sourceFieldName}属性值:M21
		if (!WoUtil.isEmpty(${r.sourceFieldName}${id})) {
			${r.target.entityName} ${r.sourceFieldName} = new ${r.target.entityName}();
			${r.sourceFieldName}.set${id}(this.${r.sourceFieldName}${id});
			po.set${r.sourceFieldNameUpperFirstChar}(${r.sourceFieldName});
		} else {
			po.set${r.sourceFieldNameUpperFirstChar}(null);
		}
	#end
	}
}
