package ${entity.getSiblingPackage("controller")};
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.entity.WoResultCode;
import com.qfedu.common.entity.WoSelectorParams;
import com.qfedu.common.exception.handler.WoControllerExceptionHandler;
import com.qfedu.psys.util.PSysConstant;
import com.qfedu.psys.util.PSysUtil;
#if (${entity.entityName} != "Menu")
import com.qfedu.psys.dto.MenuDto;
#end
import com.qfedu.psys.service.CoreService;
import ${entity.getSiblingPackage("dto")}.${entity.entityName}Dto;
import ${entity.getSiblingPackage("service")}.${entity.entityName}Service;
/**
 * ${entity.entityCN}对应控制器
 * @author cailei
 */
@Controller
@SessionAttributes(names=PSysConstant.SESSION_USER)
@RequestMapping ("/${entity.module}/${entity.entityNameLowerFirstChar}")
public class ${entity.entityName}Controller {
	private final static Logger LOG = LogManager.getLogger(${entity.entityName}Controller.class);
	
	/**
	 * 注入${entity.entityName}Service.
	 */
	@Resource
	private ${entity.entityName}Service ${entity.entityNameLowerFirstChar}Service;
	
	/**
	 * 注入MenuService
	 */
	@Resource
	private CoreService coreService;
	
	/**
	 * 注入异常处理器.
	 */
	@Resource
	private WoControllerExceptionHandler exceptionHandler;
	
	/**
	 * 加载整个${entity.entityCN}管理页面.
	 * @param map
	 * @return
	 */
	@RequestMapping ("")
	public String main (Map<String, Object> map) {
		return exceptionHandler.toLoginView(() -> {
			List<MenuDto> menus = coreService.getMenus(PSysUtil.getCurrentUser(map), "menu-${entity.module}", "menu-${entity.entityNameLowerFirstChar}");
			map.put("menus", menus);
		}, map, "${entity.module}/${entity.entityNameLowerFirstChar}");
	}
	
	/**
	 * 前端加载整个${entity.entityCN}管理页面后,会发送AJAX请求加载${entity.entityCN}列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/list")
	public String list(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<${entity.entityName}Dto> pageData = ${entity.entityNameLowerFirstChar}Service.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "${entity.module}/${entity.entityNameLowerFirstChar}-list");
	}
	
	/**
	 * 加载${entity.entityCN}创建表单.
	 * @param map
	 * @return
	 */
	@GetMapping("/create")
	public String create(Map<String, Object> map) {
		return "${entity.module}/${entity.entityNameLowerFirstChar}-create";
	}
	
	/**
	 * 提交${entity.entityCN}创建表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/create")
	@ResponseBody
	public WoResultCode create(${entity.entityName}Dto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> ${entity.entityNameLowerFirstChar}Service.create(PSysUtil.getCurrentUser(map), dto), "创建${entity.entityCN}成功！");
	}
	
	/**
	 * 加载${entity.entityCN}修改表单.
	 * @param id
	 * @param map
	 * @return
	 */
	@GetMapping("/update")
	public String update(String id, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			map.put("formData", ${entity.entityNameLowerFirstChar}Service.getById(id));
		}, map, "${entity.module}/${entity.entityNameLowerFirstChar}-update");
	}
	
	/**
	 * 提交${entity.entityCN}修改表单.
	 * @param dto
	 * @param map
	 * @return
	 */
	@PostMapping("/update")
	@ResponseBody
	public WoResultCode update(${entity.entityName}Dto dto, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> ${entity.entityNameLowerFirstChar}Service.update(dto), "修改${entity.entityCN}成功！");
	}
	
	/**
	 * 批量删除${entity.entityCN}.
	 * @param id
	 * @param map
	 * @return
	 */
	@PostMapping("/delete")
	@ResponseBody
	public WoResultCode delete(String[] id, Map<String, Object> map) {
		return exceptionHandler.toJsonView(() -> ${entity.entityNameLowerFirstChar}Service.delete(id), "删除${entity.entityCN}成功！");
	}
	
	/**
	 * 加载${entity.entityCN}选择器页面.
	 * @param params
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector")
	public String loadSelector(WoSelectorParams params, Map<String, Object> map) {
		map.put("params", params);
		return "${entity.module}/${entity.entityNameLowerFirstChar}-selector";
	}
	
	/**
	 * 前端加载整个${entity.entityCN}选择器页面后,会发送AJAX请求加载${entity.entityCN}选择器列表数据.
	 * @param searchContent
	 * @param page
	 * @param map
	 * @return
	 */
	@RequestMapping("/selector/list")
	public String selectorList(String searchContent, @RequestParam(defaultValue = "1") Long page, Map<String, Object> map) {
		return this.exceptionHandler.toAjaxErrorView(() -> {
			WoPage<${entity.entityName}Dto> pageData = ${entity.entityNameLowerFirstChar}Service.getPageData(PSysUtil.getCurrentUser(map), searchContent, page, WoPage.SIZE);
			map.put("pageData", pageData);
		}, map, "${entity.module}/${entity.entityNameLowerFirstChar}-selector-list");
	}
}
