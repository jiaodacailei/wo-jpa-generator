$(function () {
	var module = '${entity.moduleUpperFirstChar}${entity.entityName}';
	var urlPrefix = '/${entity.module}/${entity.entityNameLowerFirstChar}/';
	// 定义加载列表的函数
	function loadList (options) {
		PSys.loadList(Object.assign({
			module : module,
			url : urlPrefix + 'list'
		}, options));
	}
	// 加载列表
	loadList();
	// 设置查询按钮点击事件
	$('#btn' + module + 'Search').click (function () {
		loadList();
	});
	// 绑定列表的创建按钮点击事件
	// 此处采用$('#btn' + module + 'Create').click(function () {})是不行的,因为它是异步加载的,必须绑定到其静态加载的父元素上
	$('body').on ('click', '#btn' + module + 'Create', function () {
		PSys.loadForm({
			title : '创建',
			url : urlPrefix + 'create'
		});
	});
	// 绑定列表的批量删除按钮点击事件
	$('body').on ('click', '#btn' + module + 'BatchDelete', function () {
		PSys.deleteBatch ({
			module : module,
			url : urlPrefix + 'delete',
			fn : function () {
				loadList();
			}
		});
	});
	// 绑定列表的修改按钮点击事件
	$('body').on ('click', '.table a.button', function () {
		var id = $(this).parent().parent().find('input').val();
		PSys.loadForm({
			title : '修改',
			url : urlPrefix + 'update?id=' + id
		});
	});
	// 绑定创建表单的提交按钮点击事件
	$('body').on ('click', '#btn' + module + 'CreateFormSubmit', function () {
		PSys.submitForm({
			formSelector : '#form' + module + 'Create',
			url : urlPrefix + 'create',
			callback : function () {
				loadList();
			}
		});
	});
	// 绑定创建表单的取消按钮点击事件
	$('body').on ('click', '#btn' + module + 'CreateFormCancel', function () {
		PSys.removeTopAdminDiv();
	});
	// 绑定修改表单的提交按钮点击事件
	$('body').on ('click', '#btn' + module + 'UpdateFormSubmit', function () {
		PSys.submitForm({
			formSelector : '#form' + module + 'Update',
			url : urlPrefix + 'update',
			callback : function () {
				loadList();
			}
		});
	});
	// 绑定修改表单的取消按钮点击事件
	$('body').on ('click', '#btn' + module + 'UpdateFormCancel', function () {
		PSys.removeTopAdminDiv();
	});
	// 设置上一页按钮点击事件
	$('body').on ('click', '#btn' + module + 'LastPage', function () {
		loadList({
			pageOffset : -1
		});
	});
	// 设置下一页按钮点击事件
	$('body').on ('click', '#btn' + module + 'NextPage', function () {
		loadList({
			pageOffset : 1
		});
	});
	// 设置某一页按钮点击事件
	$('#div' + module + 'List').on ('click', ".button-group input[type=radio]", function () {
		loadList();
	});
	
	#foreach ($r in $entity.relationsM21)
	// (创建或者修改)表单中"${r.sourceFieldCN}"字段"选择"按钮点击事件
	$('body').on ('click', '#btn' + module + '${r.sourceFieldNameUpperFirstChar}Select', function () {
		var id = $("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}']").val();
		var name = $("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}']").val();
		PSys.addAdminDiv('选择${r.sourceFieldCN}').load ('/${r.target.module}/${r.target.entityNameLowerFirstChar}/selector', {
			selectedIds : id,
			selectedNames : name,
			callback : 'set' + module + '${r.sourceFieldNameUpperFirstChar}'
		}, function () {
			// 初始化字段“${r.sourceFieldCN}”的${r.target.entityCN}选择器(单选)
			init${r.target.moduleUpperFirstChar}${r.target.entityName}Selector(true);
		});
	});
	
	// (创建或者修改)表单中，字段"${r.sourceFieldCN}"的"选择"按钮点击弹出${r.target.entityCN}选择器，其确定按钮点击后的回调方法
	window.set${entity.moduleUpperFirstChar}${entity.entityName}${r.sourceFieldNameUpperFirstChar} = function (id, name) {
		$("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}']").val(id);
		$("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}']").val(name);
		// 关闭区域选择器
		return true;
	}
	#end
	
	#foreach ($r in $entity.relationsM2M)
	// (创建或者修改)表单中"${r.sourceFieldCN}"字段"选择"按钮点击事件
	$('body').on ('click', '#btn' + module + '${r.sourceFieldNameUpperFirstChar}Select', function () {
		var id = $("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}']").val();
		var name = $("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}']").val();
		PSys.addAdminDiv('选择${r.sourceFieldCN}').load ('/${r.target.module}/${r.target.entityNameLowerFirstChar}/selector', {
			selectedIds : id,
			selectedNames : name,
			callback : 'set' + module + '${r.sourceFieldNameUpperFirstChar}'
		}, function () {
			// 初始化字段“${r.sourceFieldCN}”的${r.target.entityCN}选择器(多选)
			init${r.target.moduleUpperFirstChar}${r.target.entityName}Selector(false);
		});
	});
	
	// (创建或者修改)表单中，字段"${r.sourceFieldCN}"的"选择"按钮点击弹出${r.target.entityCN}选择器，其确定按钮点击后的回调方法
	window.set${entity.moduleUpperFirstChar}${entity.entityName}${r.sourceFieldNameUpperFirstChar} = function (id, name) {
		$("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.keys.get(0).fieldNameUpperFirstChar}']").val(id);
		$("form[id^='form" + module + "'] input[name='${r.sourceFieldName}${r.target.columns.get(0).fieldNameUpperFirstChar}']").val(name);
		// 关闭选择器
		return true;
	}
	#end
});