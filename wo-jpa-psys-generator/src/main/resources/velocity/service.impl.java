package ${entity.getSiblingPackage("service.impl")};

import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.qfedu.common.entity.WoPage;
import com.qfedu.common.util.WoUtil;
import ${entity.getSiblingPackage("dto")}.${entity.entityName}Dto;
import com.qfedu.psys.dto.WoUser;
import ${entity.entityClass};
import ${entity.getSiblingPackage("repository")}.${entity.entityName}Repository;
import ${entity.getSiblingPackage("service")}.${entity.entityName}Service;

/**
 * PO实体${entity.entityName}对应的Service接口实现类.
 * @author cailei
 */
@Service
@Transactional
public class ${entity.entityName}ServiceImpl implements ${entity.entityName}Service {

	/**
	 * 使用日志打印框架.
	 */
	private final static Logger LOG = LogManager.getLogger(${entity.entityName}ServiceImpl.class);
	
	/**
	 * 注入${entity.entityName}Repository.
	 */
	@Resource // @Autowired
	private ${entity.entityName}Repository ${entity.entityNameLowerFirstChar}Repository;

	/**
	 * 列表
	 * @param currentUser 当前用户
	 * @param searchContent 查询内容文本
	 * @param page 页索引,从1开始
	 * @param size 一页的最大记录数
	 * @return
	 */
	@Override
	public WoPage<${entity.entityName}Dto> getPageData(WoUser currentUser, String searchContent, Long page, Long size) {
		// 从dao获取数据
		Pageable pageInput = PageRequest.of(page.intValue() - 1, size.intValue());
		Page<${entity.entityName}> pageData = null;
		if (WoUtil.isEmpty(searchContent)) {
			pageData = ${entity.entityNameLowerFirstChar}Repository.findAll(pageInput);
		} else {
			pageData = ${entity.entityNameLowerFirstChar}Repository.findAllBy${entity.columns.get(0).fieldNameUpperFirstChar}Like("%" + searchContent + "%", pageInput);
		}
		// 将${entity.entityName}转化为${entity.entityName}Dto
		WoPage<${entity.entityName}Dto> pr = ${entity.entityName}Dto.getPageData(pageData.getContent(), pageData.getTotalElements());
		return pr.setPage(page);
	}

	/**
	 * 创建
	 * @param currentUser 当前用户
	 * @param dto 
	 */
	@Override
	public void create(WoUser currentUser, ${entity.entityName}Dto dto) {
		${entity.entityName} po = dto.createPO();
		${entity.entityNameLowerFirstChar}Repository.save(po);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	@Override
	public ${entity.entityName}Dto getById(String id) {
		${entity.entityName} po = ${entity.entityNameLowerFirstChar}Repository.findById(id).get();
		return new ${entity.entityName}Dto(po);
	}

	/**
	 * 更新对象
	 * @param dto
	 */
	@Override
	public void update(${entity.entityName}Dto dto) {
		${entity.entityName} po = ${entity.entityNameLowerFirstChar}Repository.findById(dto.getId()).get();
		dto.updatePO(po);
		${entity.entityNameLowerFirstChar}Repository.save(po);
	}

	/**
	 * 根据ids值删除多个对象
	 * @param ids
	 */
	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			${entity.entityNameLowerFirstChar}Repository.deleteById(id);
		}
	}

}
