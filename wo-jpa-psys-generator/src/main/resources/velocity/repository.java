package ${entity.getSiblingPackage("repository")};
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import ${entity.entityClass};

/**
 * 实体${entity.entityName}基于spring-data的DAO接口.
 * @author cailei
 */
public interface ${entity.entityName}Repository extends PagingAndSortingRepository<${entity.entityName}, ${entity.keys.get(0).fieldType.name}>{
	
	Page<${entity.entityName}> findAllBy${entity.columns.get(0).fieldNameUpperFirstChar}Like(${entity.columns.get(0).fieldType.name} searchContent, Pageable pageInput);
}
